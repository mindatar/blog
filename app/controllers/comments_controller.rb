class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  
  # codigo Marco inda 
  respond_to :html, :xml, :json

    class ApplicationController < ActionController::Base

       def url_options
         { :profile => current_profile }.merge(super)
       end

    end

  # fin codigo Marco inda 
  

  # GET /comments
  # GET /comments.json
  def index
    @comments = Comment.all
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
     @post = Post.find(params[:id])		# código minda
     @comment = Comment.new(:post_id => @post.id)
     rescue ActiveRecord::RecordNotFound
           flash[:error] = "El post no existe " #fin codigo inda.
  end

  # GET /comments/new
  def new
    @comment = Comment.new
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  # POST /comments.json
  def create
    # código marco inda
    render plain: params[:post_id].inspect
    # fin codigo marco inda
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(comment_params)
    
    respond_to do |format|
      if @comment.save
        format.html { redirect_to @post, notice: 'Comment was successfully created.' }
        format.json { render :show, status: :created, location: @comment }
        format.js
      else
        format.html { render :new }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
        format.js
        
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
        
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
        
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  #
  
  def destroy
    # m. inda 
    #@post = Post.find(params[:post_id])
    #@comment = @post.comments.find ( params[:id])
    # m. inda	
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url, notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
      
    end
   # m. inda
   # private 
   #    def  comment_params
   #       params.require(:comment).permit(:commenter,:body)
   #    end
   # m. inda
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:post_id, :body)
    end
end
